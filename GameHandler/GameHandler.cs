using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour {
	[SerializeField] public Snake Snake;
	private LevelGrid lvGrid;


	public void Start() {
		
		Debug.Log("GameHander.Start");
		lvGrid = new LevelGrid (10, 10);
		Snake.Setup (lvGrid);
		lvGrid.Setup (Snake);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}